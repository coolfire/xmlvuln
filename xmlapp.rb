require 'sinatra'
require 'rexml/document'

include REXML

get '/' do
  'post stuff in the "file" param.'
end

post '/' do
  doc = Document.new(params[:file])
  doc.root.to_s
end
